# tf-azure-virtual-network

v2.0.0 updated for tf 0.13

Create a Azure virtual network and subnets

## Usage
```
module "virtual_network" {
  source              = "git::https://gitlab.com/claranet-pcp/terraform/azure/tf-azure-virtual-network?ref=v1.1.1"
  resource_group      = module.rg.resource_group_name
  resource_identifier = "my-network"
  vnet_cidr           = "10.0.0.0/16"
  location            = var.location
  gateway_subnet_cidr = "10.0.128/25"
  tags                = local.tags
}
```

## Variables
*  resource_identifier = "name of the virtual network"
*  location            = "the global network location where this virtual network is created"
*  vnet_cidr           = "The CIDR block for the Virtual Network you wish to create"
*  gateway_subnet_cidr = "The CIDR block for the gateway subnet you wish to create"
*  resource_group      = "The name of the resource group that this Virtual Network should belong to"
*  tags                = "Map of tags assigned to the resources"

## Outputs
* virtual_network_name - Name of VNET
* virtual_network_id   - The id of the virtual network itself.
* gateway_subnet_id    - Gateway subnet id
* gateway_subnet_name  - Gateay subnet name


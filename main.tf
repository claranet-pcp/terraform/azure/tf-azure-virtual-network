#
# VNET
#

resource azurerm_virtual_network network {
  name                = "vnet-${local.resource_identifier}"
  address_space       = var.vnet_cidr
  location            = var.location
  resource_group_name = var.resource_group
  dns_servers         = var.dns_servers
}

#
# Gateway Subnet
#

resource azurerm_subnet gateway {
  count                = var.gateway_subnet_cidr == "" ? 0 : 1
  name                 = "GatewaySubnet"
  resource_group_name  = var.resource_group
  virtual_network_name = azurerm_virtual_network.network.name
  address_prefixes     = [var.gateway_subnet_cidr]
}


output virtual_network_id {
  value = azurerm_virtual_network.network.id
}

output virtual_network_name {
  value = azurerm_virtual_network.network.name
}

output gateway_subnet_id {
  value = var.gateway_subnet_cidr == "" ? "" : azurerm_subnet.gateway.0.id
}

output gateway_subnet_name {
  value = var.gateway_subnet_cidr == "" ? "" : azurerm_subnet.gateway.0.name
}

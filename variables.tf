/* Environment variables */

variable location {
  description = "The region in which to create resources"
  type        = string
  default     = "uksouth"
}

variable resource_identifier {
  description = "The root module naming convention"
  type        = string
}

/* Virtual Network variables */
variable vnet_cidr {
  description = "The CIDR block for the Virtual Network you wish to create"
  type        = list
}

variable resource_group {
  description = "The name of the resource group that this Virtual Network should belong to"
  type        = string
}

variable gateway_subnet_cidr {
  description = "The CIDR block for the gateway subnet you wish to create"
  type        = string
  default     = ""
}

variable dns_servers {
  description = "List of DNS server addresses you require on the Vnet"
  type        = list
  default     = []
}

variable tags {
  description = "Map of tags to pass to resources"
  default     = {}
  type        = map
}
